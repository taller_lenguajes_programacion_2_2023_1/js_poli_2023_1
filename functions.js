function methodName(params) {
    // logic
    const conditional = typeof params === 'string';
    return conditional ? 1 : 'Hello world';
}

const result = methodName(1);
console.log(result);

// function anonym
(function (params) {
    //
    console.log(params);
})('Parameter for the method')

//arrow function
const methodWithArrow = (params) => {
    console.log(params);
    function internalFunction() { }
    internalFunction();
    const internalFunctionArrow = () => {};
    internalFunctionArrow();
}

methodWithArrow('Executing arrow function');