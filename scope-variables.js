const name = 'Daniel';
let lastName = 'Gomez';

var age = 29;

///
// name = 'Jorge'; Not possible
lastName = 'Lopez'
age = 10;

// scope 
var vvar = 60;
(function explicarVar() {
    var vvar = 40
    if (vvar === 40) {
        console.log(vvar);
        var vvar = 45;
        console.log(vvar);
    }
    console.log(vvar);
})();
console.log(vvar);




let lvar = 15;
(function explicarLet() {
    let lvar = 40;
    if (true) {
        //console.log(lvar);
        let lvar = 45;
    }
    // lvar = 10;
    console.log(lvar);
})();
console.log(lvar);

const cvar = 50;
(function explicarConst() {
    const cvar = 40;
    if (true) {
        const cvar = 45;
    }
    console.log(cvar);
})();
console.log(cvar);
