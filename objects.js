const person = {
    'name': 'Daniel',
    lastName: 'Gomez',
    age: 29,
    dni: {
        id: 123,
        type: 'cc'
    },
    calculate: () => {return 'hola'},
    notes: [1, 2, 3]
};
 
// person.any.any;

console.log(person.age);

person.age = 10;
console.log(person.age);
const property = 'dni';
console.log(person[property]);

// const contact = person;

// contact.name = 'Jorge';
// console.log(person);


// structuring (spread Operator) --- destructuing

const contact = { ...person };
contact.name = 'Jorge';
console.log(person);

// const name = person.name;
const { name: daniel, lastName, age } = person;
console.log(daniel, lastName, age);
// Array
const array = [8, 'Hola', undefined, null, true];
console.log(array[1]);
console.log(array.length);

const array2 = [ ...array ];
// const array2 = array;
// console.log(array);

array2.push(3);
console.log(array2);
array2.pop();
console.log(array2);

const [position1, position2] = array;
console.log(position1, position2);