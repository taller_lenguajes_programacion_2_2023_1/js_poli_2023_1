const typeNumber = 1233.1;
console.log(typeof typeNumber);

const typeString = 'text';
console.log(typeof typeString);
typeString.toUpperCase();

const typeBoolean = true;
console.log(typeof typeBoolean);

const typeNull = null;
console.log(typeof typeNull);

const typeUndefine = undefined;
console.log(typeof typeUndefine);

const typeBigInt = 2342423n;
console.log(typeof typeBigInt);

const typeSymbol = Symbol();
console.log(typeof typeSymbol);
console.log(typeSymbol);

const typeObj = {
    name: 'string'
};
console.log(typeof typeObj);

const typeFuction = () => {};
console.log(typeof typeFuction);