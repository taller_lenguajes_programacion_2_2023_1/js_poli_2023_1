const and = false && true;
console.log(and);

const or = false || true;
console.log(or);

const numbers = [1, 2, 3 ,4 ,5, 6]
const mathOperator = (numbers[0] > numbers[1]) && (numbers[2] >= numbers[3])
    && (numbers[2] <= numbers[3]);

//  bad practice
// const validationWihoutType = '3' == 3;
// console.log(validationWihoutType);
// const validationWihoutType = '3' != 3;
// console.log(validationWihoutType);

const string3 = '3';
const number3 = parseInt(string3);
console.log(number3 === 3);
console.log('3' !== 3);
// console.log('3' != 3);

const typeValidation = '3' === 3;
console.log(typeValidation);
const nullVariable = null;
const undefinedVariable = undefined;
const booleanString = !string3
console.log(booleanString);

if ( typeValidation ) {
    console.log('firstValidation');
} else if(nullVariable && undefinedVariable) {
    console.log('SecondValidation');
} else if(!string3) {
    console.log('ThirthValidation');
} else if (!number3 || !typeValidation) {
    console.log('fourthValidation');
}

const name = 'Daniel';
const condition = name !== 'Daniel';
console.log(condition);
if (condition) {
    console.log('Pass');
    // logic
} else {
    console.log('Dont pass');
    //logic
}

condition ? console.log('Pass') : console.log('Dont pass');

let isLongerThan22;
const age2 = 23;
if (age2 > 22 && age2 < 40) {
    isLongerThan22 = 'Yes';
} else {
    isLongerThan22 = 'No';
}
console.log(isLongerThan22);

const age = 17;
const isCarnet = true;
const conditionOfStudent = (age >= 18) && (isCarnet);
const isLongerThan18 =  conditionOfStudent ? 'Yes' : 'No';
console.log(isLongerThan18);