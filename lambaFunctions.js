const suma = (a, b) => {return a + b};
const resta = (a, b) => { return a - b};
const mulplicacion = (a, b) => { return a * b};

function operacion(a, b, method) {
    return method(a, b);
};

console.log(operacion(5, 2, suma));
console.log(operacion(5, 2, resta));
console.log(operacion(5, 2, mulplicacion));

suma(1, 2);
console.log(typeof suma);
////

const array = [1,2];
array.forEach((value, index, array) => {
    // const indexLabel = 'Hola ' + 'Mundo '
    const indexLabel = `index: ${index}`;
    const valueLabel = `value: ${value}`;
    console.log(indexLabel, valueLabel, array);
})

const persons = [
                 {name: 'daniel', lastName: 'gomez', age: 16}, 
                 {name: 'Jorge', lastName: 'gomez', age: 18}, 
                 {name: 'Jaime', lastName: 'Perez', age: 22},
                 {name: 'Jaime', lastName: 'gomez', age: 23}
                ];

persons.forEach((value, index) => {
    if(value.name === 'daniel') {
        console.log('Hello', index);
    } else {
        console.log('Hola', index);
    }
});

const resultOfFinde = persons.find((value, index) => {
    const indexLabel = `index: ${index}`;
    const valueLabel = `value: ${value.name}`;
    const mod2 = index % 2;
    console.log(indexLabel);
    console.log(value);
    console.log(mod2);
    return mod2 === 0 ? value.name === 'Pepito' : value.name === 'Jorge'
});
console.log(resultOfFinde);

const resultOfSome = persons.some((value, index) => {
    const indexLabel = `index: ${index}`;
    const valueLabel = `value: ${value.name}`;
    const mod2 = index % 2;
    console.log(indexLabel);
    console.log(value);
    console.log(mod2);
    return mod2 === 0 ? value.name === 'Pepito' : value.name === 'Jorge'
});
console.log(resultOfSome);

const firstFind = persons.find((value, index) => {
    console.log(value);
    return value.name === 'Jaime'
});
console.log(firstFind);

const personWithLastNameGomez = persons.filter((value)=> value.lastName === 'gomez');
console.log(personWithLastNameGomez);

const callbackOfFilter = (value, index) => {
    const indexLabel = `index: ${index}`;
    const valueLabel = `value: ${value.name}`;
    console.log(indexLabel);
    console.log(value);

    return value.name === 'Jaime';
}
const resultFilter = persons.filter(callbackOfFilter);
console.log(resultFilter);

// map

const resultMap = persons.map((value, index) => {
    return {
        fullName: `${value.name} ${value.lastName}`,
        age: value.age
    };
});
console.log(resultMap);

/// reduce

const resultReduce = persons.reduce((prev, act) => {
    const prevLabel = `prev: ${prev}`;
    console.log(prevLabel);
    console.log(act);
    return prev + act.age
}, 0);
console.log(resultReduce);
const prom = resultReduce / persons.length;
console.log(prom);

const resultComplexREduce = persons.reduce((prev, act) => {
    console.log(act.lastName);
    console.log(act.lastName === 'gomez');
    if(act.lastName === 'gomez') {
        return {...prev, gomez: prev['gomez'].concat({ fullName: `${act.name} ${act.lastName}`, isLongerThan18: act.age > 18})};
    } else if(act.lastName === 'Perez') {
        return {...prev, perez: prev['perez'].concat(act)};
    }
}, { gomez: [], perez: []});
console.log(resultComplexREduce);
